package main

import (
	"log"

	"gitlab.com/fogo/social-point/messaging-app-v1/cmd/daemon/bootstrap"
)

func main() {
	log.Fatal(bootstrap.Run())
}
