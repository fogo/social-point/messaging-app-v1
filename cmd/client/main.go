package main

import (
	"fmt"
	"os"

	"gitlab.com/fogo/social-point/messaging-app-v1/cmd/client/bootstrap"
)

func main() {
	if err := bootstrap.Run(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
