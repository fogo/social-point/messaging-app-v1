package main

import (
	"log"

	"gitlab.com/fogo/social-point/messaging-app-v1/cmd/server/bootstrap"
)

func main() {
	log.Fatal(bootstrap.Run())
}
