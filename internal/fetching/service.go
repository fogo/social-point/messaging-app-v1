package fetching

import (
	"context"

	"gitlab.com/fogo/social-point/messaging-app-v1/internal"
)

// Service defines the expected behaviour from a messaging service.
type Service interface {
	// FetchDevice fetches a device with the given device identifier.
	FetchDevice(context.Context, string) (comms.Device, error)
	// FetchMessages fetches all the messages with the given device identifier.
	FetchMessages(context.Context) ([]comms.Message, error)
}

// DefaultService is the default Service interface
// implementation returned by fetching.NewNewService().
type DefaultService struct {
	deviceRepository  comms.DeviceRepository
	messageRepository comms.MessageRepository
}

// NewService returns the default Service interface implementation.
func NewService(deviceRepository comms.DeviceRepository, messageRepository comms.MessageRepository) DefaultService {
	return DefaultService{
		deviceRepository:  deviceRepository,
		messageRepository: messageRepository,
	}
}

// FetchDevice implements the fetching.Service interface.
func (s DefaultService) FetchDevice(ctx context.Context, deviceID string) (comms.Device, error) {
	device, err := comms.NewDeviceID(deviceID)
	if err != nil {
		return comms.Device{}, err
	}

	return s.deviceRepository.SearchByDeviceID(ctx, device)
}

// FetchMessages implements the fetching.Service interface.
func (s DefaultService) FetchMessages(ctx context.Context) ([]comms.Message, error) {
	return s.messageRepository.SearchAll(ctx)
}
