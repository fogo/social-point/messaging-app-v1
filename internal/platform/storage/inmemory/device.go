package inmemory

import (
	"context"
	"sync"

	"gitlab.com/fogo/social-point/messaging-app-v1/internal"
)

// DeviceRepository is an in-memory implementation of the domain.DeviceRepository interface.
type DeviceRepository struct {
	sync.RWMutex
	devices map[comms.DeviceID]comms.Device
}

// NewDeviceRepository returns a new instance of an
// in-memory implementation of the domain.DeviceRepository interface.
func NewDeviceRepository() *DeviceRepository {
	return &DeviceRepository{
		devices: make(map[comms.DeviceID]comms.Device),
	}
}

// Save implements the domain.DeviceRepository interface.
func (r *DeviceRepository) Save(_ context.Context, device comms.Device) error {
	r.Lock()
	defer r.Unlock()

	if _, ok := r.devices[device.ID()]; ok {
		return comms.ErrDeviceIDAlreadyExists
	}

	r.devices[device.ID()] = device
	return nil
}

// SearchByDeviceID implements the domain.DeviceRepository interface.
func (r *DeviceRepository) SearchByDeviceID(_ context.Context, deviceID comms.DeviceID) (comms.Device, error) {
	r.RLock()
	defer r.RUnlock()

	if _, ok := r.devices[deviceID]; !ok {
		return comms.Device{}, comms.ErrDeviceNotFound
	}

	return r.devices[deviceID], nil
}
