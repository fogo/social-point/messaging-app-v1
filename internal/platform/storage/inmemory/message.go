package inmemory

import (
	"context"
	"sync"

	"gitlab.com/fogo/social-point/messaging-app-v1/internal"
)

// MessageRepository is an in-memory implementation of the comms.MessageRepository interface.
type MessageRepository struct {
	sync.RWMutex
	messages map[comms.DeviceID][]comms.Message
}

// NewMessageRepository returns a new instance of an
// in-memory implementation of the comms.MessageRepository interface.
func NewMessageRepository() *MessageRepository {
	return &MessageRepository{
		messages: make(map[comms.DeviceID][]comms.Message),
	}
}

// Save implements the comms.MessageRepository interface.
func (r *MessageRepository) Save(_ context.Context, message comms.Message) error {
	r.Lock()
	defer r.Unlock()
	r.messages[message.To()] = append(r.messages[message.To()], message)
	return nil
}

// SearchAll implements the comms.MessageRepository interface.
func (r *MessageRepository) SearchAll(_ context.Context) ([]comms.Message, error) {
	r.RLock()
	defer r.RUnlock()

	var allMessages []comms.Message

	for _, messages := range r.messages {
		allMessages = append(allMessages, messages...)
	}

	return allMessages, nil
}

// SearchByDeviceID implements the comms.MessageRepository interface.
func (r *MessageRepository) SearchByDeviceID(_ context.Context, deviceID comms.DeviceID) ([]comms.Message, error) {
	r.RLock()
	defer r.RUnlock()

	if _, ok := r.messages[deviceID]; ok {
		return r.messages[deviceID], nil
	}

	return []comms.Message{}, nil
}
