package dispatching

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"time"

	"gitlab.com/fogo/social-point/messaging-app-v1/internal"
)

// SenderBuilder is a builder function that initializes
// a new message sender instance
type SenderBuilder func(*url.URL) comms.MessageSender

// Dispatcher is the component responsible of dispatching
// all the received messages to the corresponding receiver.
type Dispatcher struct {
	deviceRepository  comms.DeviceRepository
	messageRepository comms.MessageRepository
	senderBuilder     SenderBuilder
}

// NewDispatcher returns a new instance of the dispatcher.
func NewDispatcher(
	deviceRepository comms.DeviceRepository,
	messageRepository comms.MessageRepository,
	senderBuilder SenderBuilder,

) Dispatcher {
	return Dispatcher{
		deviceRepository:  deviceRepository,
		messageRepository: messageRepository,
		senderBuilder:     senderBuilder,
	}
}

// Run starts the dispatcher's infinite loop.
func (d Dispatcher) Run(ctx context.Context) {
	go func() {
		ticker := time.NewTicker(5 * time.Second)
		for {
			select {
			case <-ticker.C:
				err := d.dispatch(ctx)
				if err != nil {
					log.Println(err)
				}

			case <-ctx.Done():
				return
			}
		}
	}()
}

func (d Dispatcher) dispatch(ctx context.Context) error {
	messages, err := d.messageRepository.SearchAll(ctx)
	if err != nil {
		return err
	}

	for _, message := range messages {
		toDevice, err := d.deviceRepository.SearchByDeviceID(ctx, message.To())
		if err != nil {
			return err
		}

		err = d.sendMessage(ctx, message, toDevice)
		if err != nil {
			return err
		}

		log.Printf("Message succesfully sent to: %s-%s\n", toDevice.ID(), toDevice.ClientIP())
	}

	return nil
}

func (d Dispatcher) sendMessage(ctx context.Context, message comms.Message, to comms.Device) error {
	endpoint, err := d.buildReceiverEndpoint(to)
	if err != nil {
		return err
	}

	sender := d.senderBuilder(endpoint)
	return sender.Send(ctx, message)
}

func (d Dispatcher) buildReceiverEndpoint(device comms.Device) (*url.URL, error) {
	return url.Parse(fmt.Sprintf("http://%s:8080/messages", device.ClientIP()))
}
