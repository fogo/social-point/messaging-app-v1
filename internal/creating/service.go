package creating

import (
	"context"
	"errors"

	"gitlab.com/fogo/social-point/messaging-app-v1/internal"
)

// Service defines the expected behaviour from a creating service.
type Service interface {
	// CreateDevice creates a new device in the system.
	CreateDevice(context.Context, string, string) error
	// CreateMessage creates a new message in the system.
	CreateMessage(context.Context, string, string, string) error
}

// DefaultService is the default Service interface
// implementation returned by creating.NewNewService().
type DefaultService struct {
	deviceRepository  comms.DeviceRepository
	messageRepository comms.MessageRepository
}

// NewService returns the default Service interface implementation.
func NewService(deviceRepository comms.DeviceRepository, messageRepository comms.MessageRepository) DefaultService {
	return DefaultService{
		deviceRepository:  deviceRepository,
		messageRepository: messageRepository,
	}
}

// CreateDevice implements the creating.Service interface.
func (s DefaultService) CreateDevice(ctx context.Context, deviceID, clientIP string) error {
	device, err := comms.NewDevice(deviceID, clientIP)
	if err != nil {
		return err
	}

	return s.deviceRepository.Save(ctx, device)
}

// CreateMessage implements the creating.Service interface.
func (s DefaultService) CreateMessage(ctx context.Context, from, to, body string) error {
	err := s.checkSenderAndReceiverAreRegistered(ctx, from, to)
	if err != nil {
		return err
	}

	message, err := comms.NewMessage(from, to, body)
	if err != nil {
		return err
	}

	return s.messageRepository.Save(ctx, message)
}

func (s DefaultService) checkSenderAndReceiverAreRegistered(ctx context.Context, from string, to string) error {
	fromID, err := comms.NewDeviceID(from)
	if err != nil {
		return err
	}

	toID, err := comms.NewDeviceID(to)
	if err != nil {
		return err
	}

	err = s.checkForRegistration(ctx, fromID)
	if err != nil {
		return err
	}

	err = s.checkForRegistration(ctx, toID)
	if err != nil {
		return err
	}
	return nil
}

func (s DefaultService) checkForRegistration(ctx context.Context, deviceID comms.DeviceID) error {
	_, err := s.deviceRepository.SearchByDeviceID(ctx, deviceID)
	if errors.Is(err, comms.ErrDeviceNotFound) {
		return comms.ErrDeviceNotRegistered
	}
	return err
}
