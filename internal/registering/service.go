package registering

import (
	"context"

	"gitlab.com/fogo/social-point/messaging-app-v1/internal"
)

// Service defines the expected behaviour from a registering service.
type Service interface {
	// Register registers a new device.
	Register(context.Context, string) error
}

// DefaultService is the default Service interface
// implementation returned by registering.NewService().
type DefaultService struct {
	registrar comms.DeviceRegistrar
}

// NewService returns the default Service interface implementation.
func NewService(registrar comms.DeviceRegistrar) DefaultService {
	return DefaultService{
		registrar: registrar,
	}
}

// Send implements the registering.Service interface.
func (s DefaultService) Register(ctx context.Context, id string) error {
	deviceID, err := comms.NewDeviceID(id)
	if err != nil {
		return err
	}

	return s.registrar.Register(ctx, deviceID)
}
