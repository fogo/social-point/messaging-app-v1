package sending

import (
	"context"

	"gitlab.com/fogo/social-point/messaging-app-v1/internal"
)

// Service defines the expected behaviour from a sending service.
type Service interface {
	// Send sends builds and sends a new message.
	Send(context.Context, string, string, string) error
}

// DefaultService is the default Service interface
// implementation returned by creating.NewService().
type DefaultService struct {
	sender comms.MessageSender
}

// NewService returns the default Service interface implementation.
func NewService(sender comms.MessageSender) DefaultService {
	return DefaultService{
		sender: sender,
	}
}

// Send implements the sending.Service interface.
func (s DefaultService) Send(ctx context.Context, from, to, body string) error {
	message, err := comms.NewMessage(from, to, body)
	if err != nil {
		return err
	}

	return s.sender.Send(ctx, message)
}
