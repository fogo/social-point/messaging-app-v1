package comms

import "context"

// Message is represented by the sender, the receiver
// and the message body.
type Message struct {
	from DeviceID
	to   DeviceID
	body string
}

func (m Message) From() DeviceID {
	return m.from
}

func (m Message) To() DeviceID {
	return m.to
}

func (m Message) Body() string {
	return m.body
}

// NewMessage returns a new Message instance.
// Returns error if there are missing requirements.
func NewMessage(from, to, body string) (Message, error) {
	fromID, err := NewDeviceID(from)
	if err != nil {
		return Message{}, err
	}

	toID, err := NewDeviceID(to)
	if err != nil {
		return Message{}, err
	}

	return Message{from: fromID, to: toID, body: body}, nil
}

// MessageRepository defines the expected behavior
// from the messages persistence layer abstraction.
type MessageRepository interface {
	// Save stores the given message.
	Save(context.Context, Message) error
	// SearchAll looks for all the stored messages.
	SearchAll(context.Context) ([]Message, error)
	// SearchByDeviceID looks for all the messages sent to the given device identifier.
	SearchByDeviceID(context.Context, DeviceID) ([]Message, error)
}

// MessageSender defines the expected behavior
// from a component capable of sending messages.
type MessageSender interface {
	// Send sends the given message.
	Send(context.Context, Message) error
}

// MessageFetcher defines the expected behavior
// from a component capable of fetching messages.
type MessageFetcher interface {
	// Fetch fetches all the messages.
	Fetch(context.Context) ([]Message, error)
}
