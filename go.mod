module gitlab.com/fogo/social-point/messaging-app-v1

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.6.1
)
